let deImgs = document.querySelectorAll(".image-to-show");
let nActiveImg = 0;
for (de of deImgs) de.style.display = "none";
deImgs[nActiveImg].style.display = "block";


let deCountDown = document.createElement('p');
document.body.append(deCountDown);


let deStop = document.createElement('button');
deStop.innerText = "Pause";
deStop.addEventListener("click", hndPause)
document.body.append(deStop);


let deResume = document.createElement('button');
deResume.innerText = "Resume";
deResume.addEventListener("click", hndResume)
document.body.append(deResume);


let tidTicker;
let nTickerTime = 5000;
let nTickerProgress = 0;
let nTickerRemain = nTickerTime;
let nTickerStep = 20;


let nFadeTime = 1000;


let nTheme;
if (!localStorage.getItem('theme')) localStorage.setItem('theme', "0");
nTheme = 1 - parseInt(localStorage.getItem('theme'));
hndSwitchTheme();
idSwitchTheme.addEventListener("click", hndSwitchTheme);


hndResume();


// The end of the main function




function hndSwitchTheme() {
    nTheme = 1 - nTheme;

    switch (nTheme) {
        case 1:
            document.body.style.backgroundColor = "#FFD6F5";
            deStop.style.backgroundColor = "#FFB94F";
            deResume.style.backgroundColor = "#FFB94F";
            break;
        case 0:
            document.body.style.backgroundColor = "#FFF";
            deStop.style.backgroundColor = "#EFEFEF";
            deResume.style.backgroundColor = "#EFEFEF";
            break;
    }

    localStorage.setItem('theme', nTheme);
}


function hndResume() {
    tidTicker = setInterval(hndTicker, nTickerStep);
    deResume.disabled = true;
    deStop.disabled = false;
}


function hndPause() {
    clearInterval(tidTicker);
    deResume.disabled = false;
    deStop.disabled = true;
    deCountDown.innerText = deCountDown.innerText + ` [paused]`;
}


function hndTicker() {
    deCountDown.innerText = `The next picture will be in ${(nTickerRemain) / 1000} sec`;

    if (nTickerProgress <= nFadeTime)
        deImgs[nActiveImg].style.opacity = nTickerProgress / nFadeTime;

    if (nTickerRemain <= nFadeTime)
        deImgs[nActiveImg].style.opacity = nTickerRemain / nFadeTime;

    nTickerProgress = (nTickerProgress + nTickerStep) % nTickerTime;
    nTickerRemain = nTickerTime - nTickerProgress;

    if (!nTickerProgress) {
        deImgs[nActiveImg].style.display = "none";
        nActiveImg = (nActiveImg + 1) % deImgs.length;
        deImgs[nActiveImg].style.opacity = 0;
        deImgs[nActiveImg].style.display = "block";
    }
}
